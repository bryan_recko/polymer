var express=require('express'),
  app = express(),
  port = process.env.port || 3000;

var path=require('path')

app.use(express.static(__dirname+'/build/default'))

app.listen(port);

console.log('Protecto Polymer con wrapper de NodeJS en puerto: '+port);

app.get('/', function functionName(req, res) {
  res.sendFile("index.html",{root:'.'})
})
